<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\CartItem;
use App\Http\Resources\CartItemCollection as CartItemCollection;
use App\Models\Order;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Coupon;
use App\Http\Requests\CouponRequest;
use App\Http\Requests\OrderRequest;
use App\Models\OrderItem;

class CartController extends Controller
{
    public function store(Request $request)
    {
        if (Auth::check()) {
            $userID = auth()->user()->id;
        }
        $cart = Cart::create([
            'id' => md5(uniqid(rand(), true)),
            'key' => md5(uniqid(rand(), true)),
            'userID' => isset($userID) ? $userID : null,

        ]);
        return response()->json([
            'Message' => 'A new cart have been created for you!',
            'cartToken' => $cart->id,
            'cartKey' => $cart->key,
        ], 201);
    }

    /**
     * Display the specified Cart.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cartKey' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 400);
        }

        $cartKey = $request->input('cartKey');
        if (Cart::where('key', $cartKey)->first()->key) {
            return response()->json([
                'cart' => Cart::where('key', $cartKey)->first()->id,
                'Items in Cart' => new CartItemCollection(Cart::where('key', $cartKey)->first()->items),
            ], 200);
        } else {

            return response()->json([
                'message' => 'The CarKey you provided does not match the Cart Key for this Cart.',
            ], 400);
        }
    }

    /**
     * Remove the specified Cart from storage.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cart $cart, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cartKey' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 400);
        }

        $cartKey = $request->input('cartKey');

        if ($cart->where('key', $cartKey)->first()->key) {
            $cart->delete();
            return response()->json([ "message" => "Delete" ], 204);
        } else {

            return response()->json([
                'message' => 'The CarKey you provided does not match the Cart Key for this Cart.',
            ], 400);
        }
    }

    /**
     * Adds Products to the given Cart;
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cart  $cart
     * @return void
     */
    public function addProducts(Cart $cart, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cartKey' => 'required',
            'productID' => 'required',
            'quantity' => 'required|numeric|min:1|max:10',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 400);
        }

        $cartKey = $request->input('cartKey');
        $productID = $request->input('productID');
        $quantity = $request->input('quantity');

        if ($cart->where('key', $cartKey)->first()->key) {

            try {
                $Product = Product::findOrFail($productID);
            } catch (ModelNotFoundException $e) {
                return response()->json([
                    'message' => 'The Product you\'re trying to add does not exist.',
                ], 404);
            }


            $cartItem = CartItem::where(['cart_id' => $cart->where('key', $cartKey)->first()->id, 'product_id' => $productID])->first();
            if ($cartItem) {
                $cartItem->quantity = $quantity;
                CartItem::where(['cart_id' => $cart->where('key', $cartKey)->first()->id, 'product_id' => $productID])->update(['quantity' => $quantity]);
            } else {
                CartItem::create(['cart_id' => $cart->where('key', $cartKey)->first()->id, 'product_id' => $productID, 'quantity' => $quantity]);
            }

            return response()->json(['message' => 'The Cart was updated with the given product information successfully'], 200);
        } else {

            return response()->json([
                'message' => 'The CarKey you provided does not match the Cart Key for this Cart.',
            ], 400);
        }
    }

    public function applyCoupon(Request $request)
    {
        $total_price = 0;
        foreach ($request->productID as $key => $value) {
            $product = Product::findOrFail($value);
            $cart_item = CartItem::where('cart_id', $request->cartID)->where('product_id', $value)->first();
            $total_price += round($cart_item->quantity * $product->price);
        }
        $coupon = Coupon::where('code', $request->code)->where('expire_date', '>=', Carbon::today())->where('expire_count', '>=', 1)->where('value', '<=', $total_price)->first();
        if($coupon) {
            $total_price = round($total_price - $coupon->value);
        } else {
            $total_price = ($total_price);
        }
        return response()->json([ "data" => $total_price ], 200);
    }


    public function getSubtotal(Request $request)
    {
        $total_price = 0;
        foreach ($request->productID as $key => $value) {
            $product = Product::findOrFail($value);
            $cart_item = CartItem::where('cart_id', $request->cartID)->where('product_id', $value)->first();
            $total_price += round($cart_item->quantity * $product->price);
        }
        return $total_price;
    }


    public function getTotalQuantity(Request $request)
    {
        $total_quantity = 0;
        foreach ($request->productID as $key => $value) {
            $product = Product::findOrFail($value);
            $cart_item = CartItem::where('cart_id', $request->cartID)->where('product_id', $value)->first();
            $total_quantity += ($cart_item->quantity);
        }
        return $total_quantity;
    }

    public function getContent(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cartKey' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
            ], 400);
        }

        $cartKey = $request->input('cartKey');
        if (Cart::where('key', $cartKey)->first()->key) {
                return new CartItemCollection(Cart::where('key', $cartKey)->first()->items);
        } else {
            return response()->json([
                'message' => 'The CarKey you provided does not match the Cart Key for this Cart.',
            ], 400);
        }
    }

    public function checkOut(OrderRequest $request)
    {

        $order = Order::create([
            'order_number'      =>  'ORD-' . strtoupper(uniqid()),
            'user_id'           => auth()->user()->id,
            'status'            =>  'pending',
            'grand_total'       =>  $this->getSubTotal($request),//cartID,productID
            'item_count'        =>  $this->getTotalQuantity($request),//cartID,productID
            'payment_status'    =>  0,
            'payment_method'    =>  1,
            'first_name'        =>  $request->first_name,
            'last_name'         =>  $request->last_name,
            'city'              =>  $request->city,
            'address'           =>  $request->address,
            'country'           =>  $request->country,
            'post_code'         =>  $request->post_code,
            'phone_number'      =>  $request->phone_number,
            'notes'             =>  $request->notes
        ]);

        if ($order) {

            $items = $this->getContent($request);//cartKey
            foreach ($items as $item) {

                $product = Product::where('id', $item->product_id)->first();

                $orderItem = new OrderItem([
                    'product_id'    =>  $product->id,
                    'quantity'      =>  $item->quantity,
                    'price'         =>  $this->getSubtotal($request)
                ]);

                $order->items()->save($orderItem);
            }
        }
        return response()->json([ "data" => "Done" ], 200);
    }

}

