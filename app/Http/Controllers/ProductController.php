<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\Coupon;
use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductResource;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function store(ProductRequest $request)
    {
        $imageName = Carbon::now()->timestamp . '.' . $request->image->getClientOriginalName();
        $request->image->move('storage/uploads', $imageName);
        if($request->images)
        {
            $imagesName = [];
            foreach ($request->images as $key => $image)
            {
                $imgName = Carbon::now()->timestamp . $key . '.' . $image->getClientOriginalName();
                $image->move('storage/uploads', $imgName);
                $imagesName[] = $imgName;

            }
            $data = Product::create([
                'name'          => $request->name,
                'slug'          => strtolower(str_replace(' ', '-', $request->name)),
                'color_id'      => $request->color_id,
                'quantity'      => $request->quantity,
                'category_id'   => $request->category_id,
                'spec_id'       => $request->spec_id,
                'image'         => $imageName,
                'images'        => implode(',',$imagesName)
            ]);
        }else {
            $data = Product::create([
                'name'          => $request->name,
                'slug'          => strtolower(str_replace(' ', '-', $request->name)),
                'color_id'      => $request->color_id,
                'quantity'      => $request->quantity,
                'category_id'   => $request->category_id,
                'spec_id'       => $request->spec_id,
                'image'         => $imageName
            ]);
        }



        return response()->json([ 'data' => $data ], 201);
    }



    public function destroy($id)
    {
        $product = Product::findOrFail($id);

        if($product->image)
        {
            unlink('storage/uploads'. '/' . $product->image);
        }
        if($product->images)
        {
            $images = explode(',',$product->images);
            foreach ($images as $image)
            {
                if($image)
                {
                    unlink('storage/uploads'. '/' . $image);
                }
            }
        }

        $product->delete();

        return response()->json(['data' => 'Done!' ], 201);
    }

    public function update(ProductRequest $request)
    {
        $product = Product::findOrFail($request->id)->update($request->all());
        return response()->json(['data' => $product ], 200);
    }

}
