<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Http\Controllers\SmsController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use \Kavenegar\Laravel\Facade as Kavenegar;
use Kavenegar\Exceptions\ApiException;
use Kavenegar\Exceptions\HttpException;
use App\Http\Requests\SignupRequest;
use App\Http\Requests\LoginRequest;
use Carbon\Carbon;


class AuthController extends Controller
{

    public function __construct()
    {
        $this->code = mt_rand(1000,9999);
    }

    public function login(LoginRequest $request)
    {
        $user = User::where('mobile_phone', $request->mobile_phone)->first();
        if(!$user || !(Hash::check($request-> password, $user-> password))){
            return response('Login failed!', 403);
        }

        return response(['token' => $user -> createToken('token') -> plainTextToken ], 200);
    }


    public function registration(SignupRequest $request)
    {

        if($this->sendCodeToReceptor($request->mobile_phone)) {
            $check = User::create([
                'name'          => $request->name,
                'email'         => $request->email,
                'mobile_phone'  => $request->mobile_phone,
                'password'      => Hash::make($request->password),
            ]);

            if($check->id===1) {
                $check->assignRole('Super Admin');
            } else {
                $check->assignRole('user');
            }
        }

        return response()->json([ 'data' => $check ], 201);
    }


    public function sendCodeToReceptor($mobile)
    {
        try {
            $message = "کد تایید شما" . " " . $this->code;
            $receptor = $mobile;
            $result = Kavenegar::Send('', $receptor, $message);
        }
        catch(ApiException $e) {
            echo $e-> errorMessage();
            abort(403);
        }
        catch(HttpException $e) {
            echo $e-> errorMessage();
            abort(403);
        }
        return true;
    }


    public function profile()
    {
        $result = auth()->user();
        return response()->json([ 'data' => $result ], 201);
    }


    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
        return response("Good By!");
    }

    public function onlineUsers()
    {
        $users = User::select("*")
            ->whereNotNull('last_seen')
            ->orderBy('last_seen', 'DESC')
            ->paginate(10);

        return response()->json($users, 200);
    }
}
