<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Color;
use App\Http\Requests\ColorRequest;
use App\Http\Resources\ColorResource;

class ColorController extends Controller
{
    public function index()
    {
        $colors = Color::all();
        return ColorResource::collection($colors);
    }


    public function store(ColorRequest $request)
    {
        $colors = Color::create([
            'name'        => $request->name,
            'code'        => $request->code,
            'category_id' => $request->category_id
        ]);
        return new ColorResource($colors);
    }

    public function update(ColorRequest $request)
    {
        $colors = Color::findOrFail($request->id)->update($request->all());
        return response()->json(["data" => "Done"], 200);
    }

    public function destroy($id)
    {
        Color::destroy($id);
        return response()->json([ "data" => "Done" ], 200);
    }

}
