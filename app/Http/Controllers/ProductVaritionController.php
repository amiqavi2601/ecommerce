<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProductVaritionRequest;
use App\Http\Resources\ProductVaritionResource;
use App\Models\ProductVaritions;

class ProductVaritionController extends Controller
{

    public function index()
    {
        $varitions = ProductVaritions::all();
        return ProductVaritionResource::collection($varitions);
    }


    public function store(ProductVaritionRequest $request)
    {
        $varitions = ProductVaritions::create([
            'product_id'    => $request->product_id,
            'color_id'      => $request->color_id,
            'price1'        => $request->price1,
            'send_time'     => $request->send_time
        ]);

        return new ProductVaritionResource($varitions);
    }

    public function update(ProductVaritionRequest $request)
    {
        $varitions = ProductVaritions::findOrFail($request->id)->update($request->all());
        return new ProductVaritionResource($varitions);
    }

    public function destroy($id)
    {
        ProductVaritions::destroy($id);
        return response()->json([ "data" => "Done" ], 200);
    }
}
