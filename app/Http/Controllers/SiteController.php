<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Specs;
use App\Models\SpecsData;
use App\Models\SpecsHeader;
use App\Models\SpecsRow;
use Illuminate\Support\Facades\DB;

class SiteController extends Controller
{
    public function compare($productId1, $productId2 = null, $productId3 = null, $productId4 = null)
    {
        $data = [];
        $products_id = array($productId1, $productId2, $productId3, $productId4);
        $products = Product::with(['specs_data', 'productVaritions'])->whereIn('id', $products_id)->get();
        if(sizeof($products)>1)
        {
            for($i=0;$i < sizeof($products);$i++)
            {
                $data[] = $products[$i]["spec_id"];
            }
            if(has_dupes($data))
            {
                $products["specs"] = DB::table('specs')
                ->join('specs_headers', 'specs_headers.spec_id', '=', 'specs.id')
                ->join('specs_rows', 'specs_rows.specs_header_id', '=', 'specs_headers.id')
                // ->join('specs_defaults', 'specs_defaults.specs_row_id', '=', 'specs_rows.id')
                ->get();
            } else {
                $products["specs"] = [];
            }
        } else {
            $products["specs"] = DB::table('specs')
                ->join('specs_headers', 'specs_headers.spec_id', '=', 'specs.id')
                ->join('specs_rows', 'specs_rows.specs_header_id', '=', 'specs_headers.id')
                // ->join('specs_defaults', 'specs_defaults.specs_row_id', '=', 'specs_rows.id')
                ->get();
        }
        return response()->json([ "data" => $products ], 200);
    }
}
