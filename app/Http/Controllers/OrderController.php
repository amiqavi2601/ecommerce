<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Requests\OrderRequest;

class OrderController extends Controller
{

    public function allOrders()
    {
        $orders = Order::all();
        return response()->json([ "data" => $orders ], 200);
    }


    public function updateOrders(OrderRequest $request)
    {
        $order = Order::findOrFail($request->id)->first();
        $order->update($request->all());
        return response()->json([ "data" => $order ], 200);
    }


    public function destroyOrders($id)
    {
        Order::destroy($id);
        return response()->json(["data" => "Done!" ], 200);
    }


    public function pendingOrders()
    {
        $orders = Order::where('status', 'pending')->orderBy('id', 'DESC')->get();
        return response()->json([ "data" => $orders ], 200);
    }

    public function processingOrders()
    {
        $orders = Order::where('status', 'processing')->orderBy('id', 'DESC')->get();
        return response()->json([ "data" => $orders ], 200);
    }

    public function completedOrders()
    {
        $orders = Order::where('status', 'completed')->orderBy('id', 'DESC')->get();
        return response()->json([ "data" => $orders ], 200);
    }

    public function declineOrders()
    {
        $orders = Order::where('status', 'decline')->orderBy('id', 'DESC')->get();
        return response()->json([ "data" => $orders ], 200);
    }
}
