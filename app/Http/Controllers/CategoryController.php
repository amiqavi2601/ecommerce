<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\ErrorHandler\Collecting;

class CategoryController extends Controller
{
    public function index()
    {
        return response()->json([ 'data' => "Done" ], 200);
    }

    public function store(CategoryRequest $request)
    {
        // dd($request);
        $category = Category::create([
            'name' => $request->name,
            'slug' => strtolower(str_replace(' ','-',$request->name)),
            'icon' => $request->icon,
            'parent_id' => $request->parent_category ? $request->parent_category:0
        ]);

        return response()->json([ 'data' => $category ], 201);
    }


    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return response()->json([ 'data' => $category ]);
    }


    public function update(CategoryRequest $request)
    {
        $category = Category::findOrFail($request->id)->update([
            'name' => $request->name,
            'slug' => strtolower(str_replace(' ','-',$request->name)),
            'icon' => $request->icon,
            'parent_id' => $request->parent_category ? $request->parent_category:0
        ]);;

        return response()->json([ 'data' => $category ], 201);
    }


    public function destroy($id)
    {
        $category = Category::destroy($id);

        return response()->json([ 'data' => $category ]);
    }


    public function getTree()
    {
        $category = Category::with('grandchildren')->where('parent_id',0)->get();

        return response()->json([ 'data' => $category ]);
    }
}
