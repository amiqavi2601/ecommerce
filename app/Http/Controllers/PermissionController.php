<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use App\Models\User;
use Spatie\Permission\Models\Role;

class PermissionController extends Controller
{
    public function roleShow($id)
    {
        $user = User::findOrFail($id);
        return UserResource::collection($user->roles);
    }

    public function adminRole($id)
    {
        $user = User::findOrFail($id)->first();
        $user->assignRole('admin');
        $user->assignRole('writer');
        $user->assignRole('editor');
        $user->assignRole('publisher');
        return new UserResource($user);
    }

    public function writerRole($id)
    {
        $user = User::findOrFail($id)->first();
        $user->assignRole('writer');
        $user->assignRole('editor');
        $user->assignRole('publisher');
        return new UserResource($user);
    }

    public function editorRole($id)
    {
        $user = User::findOrFail($id)->first();
        $user->assignRole('editor');
        $user->assignRole('publisher');
        return new UserResource($user);
    }

    public function publisherRole($id)
    {
        $user = User::findOrFail($id)->first();
        $user->assignRole('publisher');
        return new UserResource($user);
    }
}
