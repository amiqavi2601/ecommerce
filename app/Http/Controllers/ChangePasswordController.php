<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Auth;

class ChangePasswordController extends Controller
{
    public function update(Request $request)
    {
        // dd(Auth::user()->password);
        if($request->current_password === Auth::user()->password) {
            // dd($request->new_password);
            $user = User::findOrFail(Auth::user()->id);
            $user->password = Hash::make($request->new_password);
            if($request->new_password === $request->confirm_password) {
                $user->save();
                return response()->json([ "data" => $user ], 200);
            }
        }
        abort(403);
    }
}
