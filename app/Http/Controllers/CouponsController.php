<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Coupon;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\CouponRequest;
use Illuminate\Support\Facades\Validator;

class CouponsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CouponRequest $request)
    {
        $coupon = Coupon::create($request->all());

        return response()->json([ 'data' => $coupon ]);
    }


    public function show()
    {
        $coupons = Coupon::all();
        return response()->json([ "data" => $coupons ], 200);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Coupon::destroy($id);
        return response()->json([ 'data' => "delete successfull" ]);
    }

}
