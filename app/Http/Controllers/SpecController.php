<?php

namespace App\Http\Controllers;

use App\Models\Specs;
use App\Models\SpecsData;
use App\Models\SpecsDefault;
use App\Models\SpecsHeader;
use App\Models\SpecsRow;
use Illuminate\Http\Request;

class SpecController extends Controller
{
    public function specsShow()
    {
        $specs = Specs::all();
        return response()->json([ "data" => $specs ], 200);
    }

    public function specsHeaderShow()
    {
        $specs = SpecsHeader::all();
        return response()->json(["data" => $specs], 200);
    }

    public function specsRowShow()
    {
        $specs = SpecsRow::all();
        return response()->json(["data" => $specs], 200);
    }

    public function specsDataShow()
    {
        $specs = SpecsData::all();
        return response()->json(["data" => $specs], 200);
    }

    public function specsDefaultShow()
    {
        $specs = SpecsDefault::all();
        return response()->json(["data" => $specs], 200);
    }

    public function specsStore(Request $request)
    {
        $request->validate([
            'name'  => 'required'
        ]);
        $specs = Specs::create([
            'name'        => $request->name,
            'category_id' => $request->category_id
        ]);
        return response()->json(["data" => $specs], 200);
    }

    public function specsHeaderStore(Request $request)
    {
        $request->validate([
            'title' => 'required',
        ]);
        $specs = SpecsHeader::create([
            'spec_id'       => $request->spec_id,
            'title'         => $request->title,
            'description'   => $request->description,
            'is_active'     => $request->is_active
        ]);
        return response()->json(["data" => $specs], 200);
    }

    public function specsRowStore(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'icon'  => 'required'
        ]);
        $specs = SpecsRow::create([
            'specs_header_id'   => $request->specs_header_id,
            'title'             => $request->title,
            'icon'              => $request->icon,
            'description'       => $request->description,
            'prefix'            => $request->prefix,
            'postfix'           => $request->postfix
        ]);
        return response()->json(["data" => $specs], 200);
    }

    public function specsDataStore(Request $request)
    {
        $request->validate([
            'data' => 'required'
        ]);
        $specs = SpecsData::create([
            'specs_row_id'  => $request->specs_row_id,
            'product_id'    => $request->product_id,
            'category_id'   => $request->category_id,
            'data'          => $request->data,
        ]);
        return response()->json(["data" => $specs], 200);
    }

    public function specsDefaultStore(Request $request)
    {
        $request->validate([
            'value' => 'required'
        ]);
        $specs = SpecsDefault::create([
            'specs_row_id'  => $request->specs_row_id,
            'value'         => $request->value,
        ]);
        return response()->json(["data" => $specs], 200);
    }

    public function specsUpdate(Request $request)
    {
        $request->validate([
            'name'  => 'required'
        ]);
        $specs = Specs::findOrFail($request->id)->update([
            'name'        => $request->name,
            'category_id' => $request->category_id
        ]);
        return response()->json(["data" => $specs], 200);
    }

    public function specsHeaderUpdate(Request $request)
    {
        $request->validate([
            'title' => 'required',
        ]);
        $specs = SpecsHeader::findOrFail($request->id)->update([
            'spec_id'       => $request->spec_id,
            'title'         => $request->title,
            'description'   => $request->description,
            'is_active'     => $request->is_active
        ]);
        return response()->json(["data" => $specs], 200);
    }

    public function specsRowUpdate(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'icon'  => 'required'
        ]);
        $specs = SpecsRow::findOrFail($request->id)->update([
            'specs_header_id'   => $request->specs_header_id,
            'title'             => $request->title,
            'icon'              => $request->icon,
            'description'       => $request->description,
            'prefix'            => $request->prefix,
            'postfix'           => $request->postfix
        ]);
        return response()->json(["data" => $specs], 200);
    }

    public function specsDataUpdate(Request $request)
    {
        $request->validate([
            'title' => 'required',
        ]);
        $specs = SpecsData::findOrFail($request->id)->update([
            'specs_row_id'  => $request->specs_row_id,
            'category_id'   => $request->category_id,
            'data'          => $request->data,
        ]);
        return response()->json(["data" => $specs], 200);
    }

    public function specsDefualtUpdate(Request $request)
    {
        $request->validate([
            'title' => 'required',
        ]);
        $specs = SpecsDefault::findOrFail($request->id)->update([
            'specs_row_id'  => $request->spec_id,
            'value'         => $request->value,
        ]);
        return response()->json(["data" => $specs], 200);
    }

    public function specsDestroy(Request $request)
    {
        $specs = Specs::destroy($request->id);
        return response()->json(["data" => $specs], 200);
    }

    public function specsHeaderDestroy(Request $request)
    {
        $specs = SpecsHeader::destroy($request->id);
        return response()->json(["data" => $specs], 200);
    }

    public function specsRowDestroy(Request $request)
    {
        $specs = SpecsRow::destroy($request->id);
        return response()->json(["data" => $specs], 200);
    }

    public function specsDataDestroy(Request $request)
    {
        $specs = SpecsHeader::destroy($request->id);
        return response()->json(["data" => $specs], 200);
    }

    public function specsDefualtDestroy(Request $request)
    {
        $specs = SpecsHeader::destroy($request->id);
        return response()->json(["data" => $specs], 200);
    }
}
