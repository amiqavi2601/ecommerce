<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';

    protected $guarded = [];

    public function users()
    {
        return $this->belongsTo(User::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function tabledetails()
    {
        return $this->hasMany(TableDetail::class);
    }

    public function productVaritions()
    {
        return $this->hasMany(ProductVaritions::class, 'product_id');
    }

    public function colors()
    {
        return $this->hasOne(Color::class);
    }

    public function specs_data()
    {
        return $this->hasMany(SpecsData::class, 'product_id');
    }
}
