<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductVaritions extends Model
{
    use HasFactory;

    protected $fillable = ['price1', 'price2', 'send_time', 'product_id', 'color_id'];

    public function products()
    {
        return $this->hasOne(Product::class);
    }

    public function colors()
    {
        return $this->belongsTo(Color::class);
    }
}
