<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Specs extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'category_id'];


    public function products()
    {
        return $this->hasMany(Product::class, 'spec_id');
    }

    public function headers()
    {
        return $this->hasMany(SpecsHeader::class);
    }
}
