<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpecsRow extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'description', 'icon', 'prefix', 'postfix', 'specs_header_id'];

    public function headers(){
        return $this->belongsTo(SpecsHeader::class, 'specs_header_id');
    }
}
