<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpecsHeader extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'description', 'is_active', 'spec_id'];

    public function rows()
    {
        return $this->hasMany(SpecsRow::class);
    }
}
