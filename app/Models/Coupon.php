<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;

    protected $fillable = ['code', 'type', 'value', 'expire_date', 'expire_count'];
    public static function findByCode($code)
    {
        return self::where('code',$code)->first();
    }

}
