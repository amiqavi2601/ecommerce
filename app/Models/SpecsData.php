<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpecsData extends Model
{
    use HasFactory;

    protected $fillable = ['data', 'specs_row_id', 'category_id', 'product_id'];

    public function rows()
    {
        return $this->belongsTo(SpecsRow::class, 'specs_row_id');
    }
}
