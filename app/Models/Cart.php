<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Cart extends Model
{
    use HasFactory;

    public $table = "carts";

    protected $fillable = ['id', 'content', 'key', 'userID'];

    public $incrementing = false;

    public function items()
    {
        return $this->hasMany(CartItem::class, 'cart_id');
    }
}
