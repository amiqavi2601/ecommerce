<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductVaritionController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ChangePasswordController;
use App\Http\Controllers\ColorController;
use App\Http\Controllers\CouponsController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\SiteController;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\ProductResource;
use App\Http\Controllers\SpecController;
use App\Models\Product;
use App\Models\Category;
use App\Models\Specs;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', [ AuthController::class, 'login' ]);

Route::post('registration', [ AuthController::class, 'registration' ]);

Route::post('verify', [ AuthController::class, 'verify' ]);


//Route with auth login
Route::middleware(['auth:sanctum'])->group(function(){

    Route::post('logout', [ AuthController::class, 'logout' ]);
    //Admin Role
    Route::group([ 'middleware' => [ 'role:Super Admin' ]], function () {
        //Admin Api
        Route::prefix('admin')->group(function() {
            Route::post('/online-users', [ AuthController::class, 'onlineUsers' ]);

            //show User
            Route::get('/permiss/show/{id}', [ PermissionController::class, 'roleShow' ]);

            //Give Role admin
            Route::post('/permiss/{id}', [ PermissionController::class, 'adminRole' ]);

            //Give Role writer
            Route::post('/permiss/writer/{id}', [ PermissionController::class, 'writerRole' ]);

            //Give Role editor
            Route::post('/permiss/editor/{id}', [ PermissionController::class, 'editorRole']);

            //Give Role publisher
            Route::post('/permiss/publisher/{id}', [ PermissionController::class, 'publisherRole']);


            //Specs
            Route::prefix('/specs')->group(function() {
                Route::post('/view', [SpecController::class, 'specsShow']);
                Route::post('/add', [SpecController::class, 'specsStore']);
                Route::post('update', [SpecController::class, 'specsUpdate']);
                Route::post('/destroy/{id}', [SpecController::class, 'specsDestroy']);

                Route::post('/header/view', [SpecController::class, 'specsHeaderShow']);
                Route::post('/header/add', [SpecController::class, 'specsHeaderStore']);
                Route::post('/header/update', [SpecController::class, 'specsHeaderUpdate']);
                Route::post('/header/destroy/{id}', [SpecController::class, 'specsHeaderDestroy']);

                Route::post('/row/view', [SpecController::class, 'specsRowShow']);
                Route::post('/row/add', [SpecController::class, 'specsRowStore']);
                Route::post('/row/update', [SpecController::class, 'specsRowUpdate']);
                Route::post('/row/destroy/{id}', [SpecController::class, 'specsRowDestroy']);

                Route::post('/data/view', [SpecController::class, 'specsDataShow']);
                Route::post('/data/add', [SpecController::class, 'specsDataStore']);
                Route::post('/data/update', [SpecController::class, 'specsDataUpdate']);
                Route::post('/data/destroy/{id}', [SpecController::class, 'specsDataDestroy']);

                Route::post('/default/view', [SpecController::class, 'specsDefaultShow']);
                Route::post('/default/add', [SpecController::class, 'specsDefaultStore']);
                Route::post('/default/update', [SpecController::class, 'specsDefaultUpdate']);
                Route::post('/default/destroy/{id}', [SpecController::class, 'specsDefaultDestroy']);
            });

            Route::prefix('/compare')->group(function() {
                Route::get('/{productId1}', [SiteController::class, 'compare']);
                Route::get('/{productId1}/{productId2}', [SiteController::class, 'compare']);
                Route::get('/{productId1}/{productId2}/{productId3}', [SiteController::class, 'compare']);
                Route::get('/{productId1}/{productId2}/{productId3}/{productId4}', [SiteController::class, 'compare']);
            });


            //Color
            Route::prefix('/color')->group(function() {
                Route::get('/view', [ColorController::class, 'index']);

                Route::post('/add', [ColorController::class, 'store']);

                Route::post('/update', [ColorController::class, 'update']);

                Route::post('/destroy/{id}', [ColorController::class, 'destroy']);
            });

            //Product Route
            Route::prefix('/product')->group(function(){
                Route::get('/view', function () {
                    return new ProductResource(Product::all());
                });

                Route::post('/add', [ ProductController::class, 'store' ]);

                Route::post('/edit/{id}', [ ProductController::class, 'edit' ]);

                Route::post('/update', [ ProductController::class, 'update' ]);

                Route::post('/delete/{id}', [ ProductController::class, 'destroy' ]);

            });
            //end

            //ProductVarition Route
            Route::prefix('/product-vari')->group(function () {
                Route::get('/view', [ ProductVaritionController::class, 'index']);

                Route::post('/add', [ ProductVaritionController::class, 'store']);

                Route::post('/edit/{id}', [ ProductVaritionController::class, 'edit']);

                Route::post('/update', [ ProductVaritionController::class, 'update']);

                Route::post('/delete/{id}', [ ProductVaritionController::class, 'destroy']);
            });
            //end

            Route::post('/profile',[ AuthController::class, 'profile' ]);

            Route::post('/create-cart',[ CartController::class, 'store' ]);

            Route::post('/add-to-cart', [CartController::class, 'addProducts']);

            Route::post('/cartlist',[ CartController::class, 'show' ]);

            Route::post('/removecart',[ CartController::class, 'destroy' ]);

            Route::post('/add-coupon',[ CouponsController::class, 'store' ])->name('coupon.store');

            Route::post('/all-coupons',[ CouponsController::class, 'show' ]);

            Route::delete('/coupon/{id}', 'CouponsController@destroy')->name('coupon.destroy');


            //Category
            Route::prefix('/category')->group(function(){
                Route::get('/view', function() {
                    return new CategoryResource(Category::all());
                });

                Route::post('/add', [ CategoryController::class, 'store' ]);

                Route::post('/edit/{id}', [ CategoryController::class, 'edit' ]);

                Route::post('/update', [ CategoryController::class, 'update' ]);

                Route::post('/delete/{id}', [ CategoryController::class, 'destroy' ]);

                Route::post('/tree', [ CategoryController::class, 'getTree' ]);

            });
            //end

            Route::prefix('checkout')->group(function () {
                Route::get('', [ CartController::class, 'show']);
                Route::post('/order', [ CartController::class, 'checkOut']);
            });
        });//end Amin api

    });
    //end Admin Role

    //User Role
    Route::group([ 'middleware' => [ 'role:Super Admin|user' ]], function() {
        Route::prefix('user')->group(function() {
            Route::get('/product/view', function () {
                return new ProductResource(Product::all());
            });
            Route::post('/profile', [ AuthController::class, 'profile' ]);

            Route::post('/create-cart', [ CartController::class, 'store' ]);

            Route::post('/add-to-cart', [CartController::class, 'addProducts']);

            Route::post('/cartlist', [ CartController::class, 'show' ]);

            Route::post('/removecart', [ CartController::class, 'destroy' ]);

            Route::post('/checkout', [ CartController::class, 'checkout' ]);


            Route::get('/category/view', function () {
                return new CategoryResource(Category::all());
            });

            Route::post('/change-password', [ ChangePasswordController::class, 'update' ]);
        });
    });


});
