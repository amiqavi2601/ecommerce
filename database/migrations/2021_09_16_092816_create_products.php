<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug')->unique();
            $table->unsignedBigInteger('quantity')->default(1);
            $table->integer('price')->nullable();
            $table->integer('discount_price')->nullable();
            $table->smallInteger('show')->default(1);
            $table->string('image')->nullable();
            $table->text('images')->nullable();
            $table->foreignId('category_id')->constrained();
            $table->text('long_description')->nullable();
            $table->string('short_description')->nullable();
            $table->string('product_brand')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
