<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecsRowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specs_rows', function (Blueprint $table) {
            $table->id();
            $table->foreignId('specs_header_id')->constrained();
            $table->string('title');
            $table->string('icon');
            $table->text('description')->nullable();
            $table->string('prefix')->nullable();
            $table->string('postfix')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specs_rows');
    }
}
